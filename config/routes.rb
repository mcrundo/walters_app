Rails.application.routes.draw do

  root 'welcome#index'

  resources :works

  get '/works' => 'works#index'
  get '/search' => 'works#search'

  resources :users, except: [:index] do
    resources :favorites, except: [:edit, :show, :update]
  end


  resources :sessions, only: [:create]

  get '/signup' => 'users#new'
  get '/signin' => 'sessions#new'
  delete '/signout' => 'sessions#destroy'


end
