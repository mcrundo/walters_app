require 'spec_helper'

describe "a user can navigate and" do
  let(:user) { FactoryGirl.create(:user) }

  it "a user can search for a work by a creator" do
    visit '/'
    fill_in "Search", with: "monet"
    click_button("search")
    expect(page).to have_content "monet"
    expect(page).to have_content "Springtime"
  end

  it "a user can view a work's show view" do
    visit '/works/10078'
    expect(page).to have_content "Monet"
  end

  def login(user)
    visit "/signin"
    fill_in "Email", with: user.email
    fill_in "Password", with: user.password
    click_button "Sign in"
  end

end
