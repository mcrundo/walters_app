# MuSM
[musm-app.herokapp.com](http://musm-app.herokuapp.com/)

### GA WDI DC April 2014, Project 1

### Overview

**MuSM** is a virtual museum and learning application. MuSM was developed as a Rails project for the Spring 2014 Web Development Immersive course at General Assembly.

### Technologies Used

* Ruby 2.1.1
* Ruby on Rails 4.1.1
* PostgreSQL Database
* Authentication & Authorization from scratch using [bcrypt-ruby](http://bcrypt-ruby.rubyforge.org/)
* [Walters Museum API](https://github.com/WaltersArtMuseum/walters-api), [Google Maps Embed API](https://developers.google.com/maps/documentation/embed/) & [HTTParty](https://github.com/jnunemaker/httparty)
* Testing using using [rspec-rails](https://github.com/rspec/rspec-rails), [capybara](https://github.com/jnicklas/capybara), [shoulda-matchers](https://github.com/thoughtbot/shoulda-matchers) and [factory_girl_rails](https://github.com/thoughtbot/factory_girl_rails)

### User Stories Completed

* As a user I should be able to search for content based on a set of parameters
* As a user I should be able to become a member
* As a member I should be able to save a work of art and access it
* A sample of the collection should appear on the welcome page
* Upcoming exhibitions should appear on the welcome page


---
###### Written by Matt Rundo
