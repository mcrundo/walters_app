class AddObjectIDtoWorks < ActiveRecord::Migration
  def change
    add_column :works, :object_id, :string
  end
end
