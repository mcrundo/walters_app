class CreateFavorites < ActiveRecord::Migration
  def change
    create_table :favorites do |t|
      t.string :walters_id
      t.references :user
    end
  end
end
