module ApplicationHelper

  # Walters object general search
  def index_api_path(params='')
    "http://api.thewalters.org/v1/objects.json?apikey=#{ENV['WALTERS_API_KEY']}#{params}"
  end

  # Walters exhibits search
  def exhibit_api_path(params="")
    "http://api.thewalters.org/v1/exhibitions.json?apikey=#{ENV['WALTERS_API_KEY']}#{params}"
  end

  # Walters show object by id
  def show_api_path(params='')
    "http://api.thewalters.org/v1/objects/#{params}?apikey=#{ENV['WALTERS_API_KEY']}&pageSize=60"
  end

  # Reverse geocode object's lat/long
  def geocode_api_path(params='')
    "https://maps.googleapis.com/maps/api/geocode/json?latlng=#{params}"
  end

  # Walters object image search by id
  def image_api_path(params="")
    "http://api.thewalters.org/v1/objects/#{params}/images.json?apikey=#{ENV['WALTERS_API_KEY']}"
  end

  # Google Maps search
  # def map_object(params="")
  #   "https://www.google.com/maps/embed/v1/place?key=#{ENV['GOOGLE_MAPS_API_KEY']}&q=#{params}&zoom=6"
  # end

  # Removes accents from characters for object creator search
  def creator_accent_remove
    creator_accent_remove = ".tr(
'ÀÁÂÃÄÅàáâãäåĀāĂăĄąÇçĆćĈĉĊċČčÐðĎďĐđÈÉÊËèéêëĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħÌÍÎÏìíîïĨĩĪīĬĭĮįİıĴĵĶķĸĹĺĻļĽľĿŀŁłÑñŃńŅņŇňŉŊŋÒÓÔÕÖØòóôõöøŌōŎŏŐőŔŕŖŗŘřŚśŜŝŞşŠšſŢţŤťŦŧÙÚÛÜùúûüŨũŪūŬŭŮůŰűŲųŴŵÝýÿŶŷŸŹźŻżŽž',
'AAAAAAaaaaaaAaAaAaCcCcCcCcCcDdDdDdEEEEeeeeEeEeEeEeEeGgGgGgGgHhHhIIIIiiiiIiIiIiIiIiJjKkkLlLlLlLlLlNnNnNnNnnNnOOOOOOooooooOoOoOoRrRrRrSsSsSsSssTtTtTtUUUUuuuuUuUuUuUuUuUuWwYyyYyYZzZzZz').gsub(' ', '+')"
  end

  # Select a random query from the array
  def random_query
    random_query = ["French", "American", "Italian", "Indian", "Dutch", "Spanish"].sample
  end

  # Get the random queries Walters API response
  def random_results
    # Get random item
    random_query
    random_response = HTTParty.get(index_api_path "&creator=#{random_query}&pageSize=60")
    random_hash = random_response
    random_results = random_hash["Items"]
  end



end
