class FavoritesController < ApplicationController

  def index
  end

  def create
    if current_user
      current_user.favorites.create(favorite_params)
      redirect_to current_user
    else
      redirect_to root_path
    end
  end

  def destroy
    @user.favorite.destroy
    redirect_to current_user
  end

  private

    def favorite_params
      params.require(:favorite).permit(:walters_id, :walters_title, :walters_image_url)
    end

end
