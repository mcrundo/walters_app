class WelcomeController < ApplicationController

  def index

    # Popular favorited works
    @favorited = Favorite.all
    @favorites_array = [].take(5)
    @favorited.each do |favorite|
        @favorites_array << favorite.walters_id
    end

    # Get random item
    @random_query = ["American", "1860", "1840", "1900", "1920"].sample
    random_hash = HTTParty.get(index_api_path "&creator=#{@random_query}&pageSize=60")
    @random_results = random_hash["Items"]

    # Get current exhibitions
    current_year = Time.now.strftime("%Y")
    response = HTTParty.get(exhibit_api_path "&endYear=#{current_year}")
    @response_hash = response["Items"]
  end


end
