class WorksController < ApplicationController

  def search
    work = params[:q].gsub(" ", "+")
    response = HTTParty.get(index_api_path "&creator=#{work}&pageSize=60")
    response_hash = response
    @search_query = work
    @search_results = response_hash["Items"]
  end

  def index
    response = HTTParty.get(index_api_path "&creator=#{work}&pageSize=60")
    response_hash = response
    @search_results = response_hash["Items"]
  end

  def show
    @favorite = Favorite.new
    object_id = params[:id].gsub(" ", "+")
    response_hash = HTTParty.get(show_api_path "#{object_id}")
    # response_hash = response
    @search_results = response_hash["Data"]
    @creator_accent_remove = creator_accent_remove
    @work_long_lat = "#{@search_results['Geographies'][0]['LatitudeNumber']},#{@search_results['Geographies'][0]['LongitudeNumber']}"
    # work_location_response = HTTParty.get(geocode_api_path "#{@work_long_lat}")
    # @work_location = work_location_response["results"][2]["address_components"][1]["long_name"]
  end

end
